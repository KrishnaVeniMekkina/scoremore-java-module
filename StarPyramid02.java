public class StarPyramid02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print(starPattern(6));

	}
	public static String starPattern(int rows) {
		 //ADD YOUR CODE HERE
		if(rows < 0) {
			return "-1";
		} else if (rows == 0) {
			return "-2";
		} else {
			String str = "";
			for(int i = 1; i <= rows ; i++) {
				for(int s = 1 ; s <= rows - i ; s++) {
					str += " ";
				}
				for(int j = 1; j <= 2*i - 1;j++) {
					str += "*";
				}
				str += "\n";
			}
			return str;
		}
	}
}