import java.io.*;

public class CopyFile01 {
    public static void main(String[] args) {
        String source = "SourceFile.txt";
        String dest = "DestinationFile.txt";
        boolean status = copyFile(source, dest);
        System.out.println(status);
    }
    
    public static boolean copyFile(String source, String dest) {
        // ADD YOUR CODE HERE
	
	try{
		InputStream is = new FileInputStream(source);
		OutputStream os = new FileOutputStream(dest);
		byte[] buffer = new byte[1024];
		int length;
		while ((length = is.read(buffer)) > 0) {
			os.write(buffer, 0, length);
		}
		is.close();
		os.close(); 
		return true;  
	}
	catch(IOException e){}
	return false;

	
    }
}