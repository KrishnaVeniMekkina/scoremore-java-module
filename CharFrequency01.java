import java.util.*;
public class CharFrequency01 {
	public static void main(String[] args) {
		Map<Character, Integer> freqCount = frequencyCount("ELEMENTS");
        System.out.println(freqCount);
        
        for (Character key : freqCount.keySet()) {
            System.out.println(key + " " + freqCount.get(key));
        }  
        
	}
    public static Map<Character, Integer> frequencyCount(String str) {
       //ADD YOUR CODE HERE
       Map<Character,Integer>freqCount = new LinkedHashMap<Character,Integer>();
       if(str == null){
         return freqCount;
       }
       str = str.trim();
       if(str.length() == 0){
          return freqCount;
       }
       str = str.toUpperCase();
       for(char ch : str.toCharArray()){
           if(Character.isLetter(ch)){
             if(freqCount.containsKey(ch)){
                freqCount.put(ch,(Integer)freqCount.get(ch) + 1);
             }else{
                freqCount.put(ch,1);
             }
          }
       }
       return freqCount;
    }
}