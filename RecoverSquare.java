import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 */
public class Main {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        RecoverSquare solver = new RecoverSquare();
        int testCount = Integer.parseInt(in.next());
        for (int i = 1; i <= testCount; i++)
            solver.solve(i, in, out);
        out.close();
    }

    static class RecoverSquare {
        public void solve(int testNumber, InputReader in, PrintWriter out) {
            int n = in.nextInt();
            int m = in.nextInt();
            Graph graph = new Graph(n * n, m);
            boolean bad = false;
            for (int i = 0; i < m; i++) {
                int u = in.nextInt() - 1;
                int v = in.nextInt() - 1;
                graph.addUndirectedEdge(u, v);
            }
            if (bad) {
                out.println(-1);
                return;
            }
            int[] position = solveTheProblem(n, m, graph);
            if (position == null) {
                out.println(-1);
                return;
            }
            int[][] print = new int[n][n];
            for (int i = 0; i < n * n; i++) {
                int x = position[i] / n;
                int y = position[i] % n;
                print[x][y] = i;
            }
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (j > 0) {
                        out.print(' ');
                    }
                    out.print(print[i][j] + 1);
                }
                out.println();
            }
        }

        boolean edgeCountOK(int n, int m) {
            int needSumDeg = 0;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    int needDeg = getDeg(n, i, j);
                    needSumDeg += needDeg;
                }
            }
            return needSumDeg == 2 * m;
        }

        private int getDeg(int n, int i, int j) {
            int deg = 0;
            for (int dx = -2; dx <= 2; dx++) {
                for (int dy = -2; dy <= 2; dy++) {
                    if (i + dx < 0 || i + dx >= n || j + dy < 0 || j + dy >= n) {
                        continue;
                    }
                    int d = Math.abs(dx) + Math.abs(dy);
                    if (d != 1 && d != 2) {
                        continue;
                    }
                    ++deg;
                }
            }
            return deg;
        }

        private int getPrevCells(int n, int i, int j) {
            int deg = 0;
            for (int dx = -2; dx <= 2; dx++) {
                for (int dy = -2; dy <= 2; dy++) {
                    if (dx > 0) {
                        continue;
                    }
                    if (dx == 0 && dy >= 0) {
                        continue;
                    }
                    if (i + dx < 0 || i + dx >= n || j + dy < 0 || j + dy >= n) {
                        continue;
                    }
                    int d = Math.abs(dx) + Math.abs(dy);
                    if (d != 1 && d != 2) {
                        continue;
                    }
                    ++deg;
                }
            }
            return deg;
        }

        private boolean checkSolution(int n, Graph graph, int[] position) {
            if (position == null) {
                return false;
            }
            for (int u = 0; u < n * n; u++) {
                int uX = position[u] / n;
                int uY = position[u] % n;
                for (int e = graph.firstOutbound(u); e >= 0; e = graph.nextOutbound(e)) {
                    int v = graph.destination(e);
                    int vX = position[v] / n;
                    int vY = position[v] % n;
                    int dist = Math.abs(uX - vX) + Math.abs(uY - vY);
                    if (dist != 1 && dist != 2) {
                        return false;
                    }
                }
            }
            return true;
        }

        private int[] solveTheProblem(int n, int m, Graph graph) {
            if (!edgeCountOK(n, m)) {
                return null;
            }
            if (n <= 2) {
                boolean[][] edge = new boolean[n * n][n * n];
                for (int u = 0; u < n * n; u++) {
                    for (int e = graph.firstOutbound(u); e >= 0; e = graph.nextOutbound(e)) {
                        int v = graph.destination(e);
                        edge[u][v] = true;
                    }
                }
                for (int i = 0; i < n * n; i++) {
                    for (int j = 0; j < i; j++) {
                        if (!edge[i][j]) {
                            return null;
                        }
                    }
                }
                int[] res = new int[n * n];
                for (int i = 0; i < n * n; i++) {
                    res[i] = i;
                }
                return res;
            }
            int[] deg = new int[n * n];
            for (int i = 0; i < n * n; i++) {
                for (int e = graph.firstOutbound(i); e >= 0; e = graph.nextOutbound(e)) {
                    ++deg[i];
                }
                if (deg[i] >= 13) {
                    return null;
                }
            }
            int[] overlap = new int[2 * m];
            int[] was = new int[n * n];
            Arrays.fill(was, -1);
            for (int u = 0; u < n * n; u++) {
                for (int e = graph.firstOutbound(u); e >= 0; e = graph.nextOutbound(e)) {
                    int v = graph.destination(e);
                    was[v] = u;
                }
                for (int e = graph.firstOutbound(u); e >= 0; e = graph.nextOutbound(e)) {
                    int v = graph.destination(e);
                    for (int e2 = graph.firstOutbound(v); e2 >= 0; e2 = graph.nextOutbound(e2)) {
                        int w = graph.destination(e2);
                        if (was[w] == u) {
                            overlap[e]++;
                        }
                    }
                }
            }
            int[] position = new int[n * n];
            int[] nodeAt = new int[n * n];

            Arrays.fill(position, -1);
            Arrays.fill(nodeAt, -1);

            if (doFirstThreeCells(n, graph, deg, overlap, position, nodeAt)) return null;

            int[] prevCellsNeighbourCount = new int[n * n];
            for (int i = 0; i < 3; i++) {
                int u = nodeAt[i];
                for (int e = graph.firstOutbound(u); e >= 0; e = graph.nextOutbound(e)) {
                    int v = graph.destination(e);
                    prevCellsNeighbourCount[v]++;
                }
            }
            for (int row = 0; row < n; row++) {
                for (int i = 0; i < n; i++) {
                    if (row == 0 && i < 3) {
                        continue;
                    }
                    int needDeg = getDeg(n, row, i);
                    int needPrevCellsNeighbourCount = getPrevCells(n, row, i);

                    int lookFor;
                    if (row > 0) {
                        lookFor = i == 0 ? nodeAt[(row - 1) * n + i] : nodeAt[(row - 1) * n + i - 1];
                    } else {
                        lookFor = nodeAt[row * n + i - 1];
                    }
                    for (int e = graph.firstOutbound(lookFor); e >= 0; e = graph.nextOutbound(e)) {
                        int v = graph.destination(e);
                        if (position[v] >= 0) {
                            continue;
                        }
                        if (deg[v] != needDeg) {
                            continue;
                        }
                        int neighbourCount = 0;
                        for (int e2 = graph.firstOutbound(v); e2 >= 0; e2 = graph.nextOutbound(e2)) {
                            int w = graph.destination(e2);
                            if (position[w] >= 0) {
                                ++neighbourCount;
                            }
                        }
                        if (neighbourCount != needPrevCellsNeighbourCount) {
                            continue;
                        }
                        position[v] = row * n + i;
                        nodeAt[row * n + i] = v;
                        break;
                    }

                    if (nodeAt[row * n + i] < 0) {
//                    for (int j = 0; j < 3; j++) {
//                        System.err.println(nodeAt[j]);
//                    }
//                    System.err.println("failed at "+row+","+i);
                        return null;
                    }

                    for (int e = graph.firstOutbound(nodeAt[row * n + i]); e >= 0; e = graph.nextOutbound(e)) {
                        int v = graph.destination(e);
                        ++prevCellsNeighbourCount[v];
                    }
                }
            }

            if (!checkSolution(n, graph, position)) {
                return null;
            }

            return position;
        }

        private boolean doFirstThreeCells(int n, Graph graph, int[] deg, int[] overlap, int[] position, int[] nodeAt) {
            int degCorner = getDeg(n, 0, 0);
            for (int i = 0; i < n * n; i++) {
                if (deg[i] == degCorner) {
                    position[i] = 0;
                    nodeAt[0] = i;
                    break;
                }
            }
            if (nodeAt[0] < 0) {
                return true;
            }
            int degSide1 = getDeg(n, 0, 1);
            int overlap01 = 3;
            for (int e = graph.firstOutbound(nodeAt[0]); e >= 0; e = graph.nextOutbound(e)) {
                int v = graph.destination(e);
                if (overlap[e] != overlap01) {
                    continue;
                }
                if (deg[v] == degSide1) {
                    nodeAt[1] = v;
                    position[v] = 1;
                    break;
                }
            }
            if (nodeAt[1] < 0) {
                return true;
            }
            int degSide2 = getDeg(n, 0, 2);
            int overlap02 = 2;
            for (int e = graph.firstOutbound(nodeAt[0]); e >= 0; e = graph.nextOutbound(e)) {
                int v = graph.destination(e);
                if (overlap[e] != overlap02) {
                    continue;
                }
                if (!hasEdge(graph, nodeAt[1], v)) {
                    continue;
                }
                if (deg[v] == degSide2) {
                    nodeAt[2] = v;
                    position[v] = 2;
                    break;
                }
            }
            if (nodeAt[2] < 0) {
                return true;
            }
            return false;
        }

        boolean hasEdge(Graph graph, int u, int v) {
            for (int e = graph.firstOutbound(u); e >= 0; e = graph.nextOutbound(e)) {
                if (graph.destination(e) == v) {
                    return true;
                }
            }
            return false;
        }

    }

    static class InputReader {
        public BufferedReader br;
        StringTokenizer st;

        public InputReader(InputStream stream) {
            br = new BufferedReader(new InputStreamReader(stream));
        }

        public String next() {
            return nextToken();
        }

        public String nextToken() {
            while (st == null || !st.hasMoreTokens()) {
                String line = null;
                try {
                    line = br.readLine();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                if (line == null) {
                    return null;
                }
                st = new StringTokenizer(line);
            }
            return st.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(nextToken());
        }

    }

    static class Graph {
        private int[] firstOutbound;
        private int[] nextOutbound;
        private int[] to;
        private int vertexCount;
        private int edgeCount;

        public Graph(int vertices) {
            this(vertices, 2 * vertices - 2);
        }

        public Graph(int vertexCount, int maxEdges) {
            this.vertexCount = vertexCount;
            this.firstOutbound = new int[vertexCount];
            this.nextOutbound = new int[maxEdges];
            this.to = new int[maxEdges];
            this.edgeCount = 0;
            Arrays.fill(firstOutbound, -1);
        }

        private void ensureCapacity() {
            if (edgeCount == to.length) {
                int newLength = Math.max(10, 2 * to.length);
                to = Arrays.copyOf(to, newLength);
                nextOutbound = Arrays.copyOf(nextOutbound, newLength);
            }
        }

        public void addDirectedEdge(int u, int v) {
            ensureCapacity();
            to[edgeCount] = v;
            nextOutbound[edgeCount] = firstOutbound[u];
            firstOutbound[u] = edgeCount;
            ++edgeCount;
        }

        public void addUndirectedEdge(int u, int v) {
            addDirectedEdge(u, v);
            addDirectedEdge(v, u);
        }

        public int firstOutbound(int u) {
            return firstOutbound[u];
        }

        public int nextOutbound(int edgeId) {
            return nextOutbound[edgeId];
        }

        public int destination(int edgeId) {
            return to[edgeId];
        }

    }
}