public class MaskEmailID01 {
    public static void main(String[] args) {
        String email = "testmail@mailme.com";
        System.out.println(maskMailID(email));
    }

    public static String maskMailID(String email) {
		int i=0;
		char[] a = email.toCharArray();
		for(i=2;a[i] != '@';i++){
			a[i] = 'X';
		}
		String str = new String(a);
		return str;			
    }
}