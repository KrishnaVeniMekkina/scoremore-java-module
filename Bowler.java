public class Bowler {
	public String name;
	public int wickets;
    public int matches;
    public int balls_bowled;
    public int runs_conceded;
    
    public Bowler() {
    	
    }

    public Bowler(String name,int wickets,int matches,int balls_bowled,int runs_conceded) { 
    	this.name = name;
    	this.wickets = wickets;
    	this.matches = matches;
    	this.balls_bowled = balls_bowled;
    	this.runs_conceded = runs_conceded;
    }
    public void computeBowlingAverage() {
    	int avg = runs_conceded/wickets;
        if(runs_conceded > 0 && wickets > 0){
    	System.out.println("Name:"+name);
    	System.out.println("bowling_avg="+avg);
        }
        else if(runs_conceded < 0 || wickets < 0) {
        	System.out.println("Error");
        }
        else if(runs_conceded > 0 || wickets > 0 && matches == 0) {
        	System.out.println("Error");
        }
    }
    public void showStatistics() {
    	if(wickets > 0 && matches > 0 && balls_bowled > 0 && runs_conceded> 0) {
    		System.out.println("Name="+name);
    		System.out.println("wickets="+wickets);
    		System.out.println("matches="+matches);
    		System.out.println("balls_bowled="+balls_bowled);
    		System.out.println("runs_conceded="+runs_conceded);
    	}
    	else if(balls_bowled > 0 || runs_conceded > 0 && matches == 0) {
    		System.out.println("Error");
    	}
    	else {
    		System.out.println("Error");
    	}
    }
    public void computeStrikeRate() {
    	int rate = runs_conceded/balls_bowled;
    	if(wickets > 0 && matches > 0 && balls_bowled > 0 && runs_conceded> 0) {
    		System.out.println("Name:"+name);
    		System.out.println(" Strike_rate="+rate);
    	}
    	else if(balls_bowled > 0 || runs_conceded > 0 && matches == 0) {
    		System.out.println("Error");
    	}
    	else {
    		System.out.println("Error");
    	}
    	
    }

	public static void main(String[] args) {
		Bowler bowler = new Bowler("Sachin",10,5,750,463);
		bowler.computeBowlingAverage();
		bowler.showStatistics();
		bowler.computeStrikeRate();
	}
}