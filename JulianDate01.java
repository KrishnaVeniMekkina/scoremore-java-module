public class JulianDate01 {
    public static void main(String[] args) {
        String date = "23-Jan-2016";
        System.out.println(dateFormat(date));
    }
   
    public static String dateFormat(String date) {
        String[] dateparts = date.split("-");
        int dd = Integer.parseInt(dateparts[0]);
        int mon = convertMMMtoMM(dateparts[1]);
        int yyyy = Integer.parseInt(dateparts[2]);
        return yyyy + julianDate(dd,mon);
    }
   
   public static String julianDate(int dd, int mon) {
      int[] MONTHS = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};
      int jd = MONTHS[mon - 1] + dd;
      String day = "" + jd;
      if(jd <= 9) {
          day = "00" + jd;
      } else if(jd <= 99) {
          day = "0" + jd;
      }
      return day;
   }
   
   public static int convertMMMtoMM(String mon) {
      String months = "JANFEBMARAPRMAYJUNJULAUGSEPOCTNOVDEC";
      mon = mon.substring(0,3);
      mon = mon.toUpperCase();
      return ((months.indexOf(mon) / 3) + 1);
   }
}