import java.util.*;
public class Anagrams01 {
    public static void main(String[] args) {
        String[] words = {"listen", "silent", "elbow", "part", "panel", "trap", "tensil", "alter", "later", "below"};
        List<String> anagrams = findAnagrams(words);
        for (String word : anagrams) {
            System.out.println(word);
        }
    }

    public static List<String> findAnagrams(String[] words) {
          List<String>anagrams = new ArrayList<String>();
          Map<String,String> anagrams_dict = new LinkedHashMap<String,String>();
          for(String word : words){
             word = word.toLowerCase();
             String key = generateKey(word);
             if(anagrams_dict.containsKey(key)){
                 anagrams_dict.put(key,anagrams_dict.get(key)+","+word);
             }else{
                 anagrams_dict.put(key,word);
             }
          }
          for(String key : anagrams_dict.keySet()){
             String val = anagrams_dict.get(key);
             if(val.indexOf(",") > 0){
               anagrams.add(val);
             }
         }
         return anagrams;
    }
    public static String generateKey(String word){
           char[] letters = word.toCharArray();
           Arrays.sort(letters);
           return new String(letters);
   }
}